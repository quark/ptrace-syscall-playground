all: hello trace

trace: trace.cc
	g++ $^ -o $@

hello: hello.o
	gcc -nostdlib $^ -o $@

hello.o: hello.asm
	nasm -f elf64 $^ -o $@
