SECTION .data
  msg:  db "hello world", 10

SECTION .text
  global _start

_start:
  mov eax, 4    ; write (x86) (in amd64, 4 is stat)
  mov ebx, 2    ; int fd, 2 is stderr
  mov ecx, msg  ; const void * buf
  mov edx, 12   ; size_t count, sizeof(buf)
  int 0x80

  mov rax, 1    ; write (amd64)
  mov rdi, 2
  mov rsi, msg
  mov rdx, 12
  syscall

  mov eax, 123456
  int 0x3

  mov ebx, 0    ; exit code
  mov eax, 1    ; exit (x86) (in amd64, 1 is write)
  int 0x80
