#include <cstdio>
#include <cstdlib>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <cstring>


const char signals[][10] = {"", "SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP", "SIGIOT", "", "SIGFPE", "SIGKILL", "SIGBUS", "SIGSEGV", "SIGSYS", "SIGPIPE", "SIGALRM", "SIGTERM", "SIGURG", "SIGSTOP", "SIGTSTP", "SIGCONT", "SIGCHLD", "SIGTTIN", "SIGTTOU", "", "SIGXCPU", "SIGXFSZ", "SIGVTALRM", "SIGPROF", "", "", "SIGUSR1", "SIGUSR2"};

const char * signal_str(unsigned int signal) {
  static char buf[1024];
  if (signal < (sizeof(signals) / sizeof(signals[0]))) {
    snprintf(buf, sizeof buf, "%d (%s)", signal, signals[signal]);
  } else {
    snprintf(buf, sizeof buf, "%d", signal);
  }
  return buf;
}

// int 0x3 siginfo
// 05 00 00 00 00 00 00 00 80 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
//                                                 ~~~~~~~~~~~ ~~~~~~~~~~~
//                                                 si_pid      si_uid
// int 0x80 siginfo (enter, exit)
// 05 00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 f4 57 00 00 e8 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// 05 00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 f4 57 00 00 e8 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// syscall siginfo (enter, exit)
// 05 00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 f4 57 00 00 e8 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// 05 00 00 00 00 00 00 00 05 00 00 00 00 00 00 00 f4 57 00 00 e8 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00


void print_siginfo(siginfo_t siginfo) {
  printf("  siginfo =");
  for (size_t i = 0; i < sizeof(siginfo_t); ++i) {
    printf(" %02x", ((unsigned char *)&siginfo)[i] );
  }
  puts("");
}

int main(int argc, char const *argv[]) {
  pid_t pid = fork();
  if (pid) {
    // parent
    printf("pid = 0x%x\n", (int) pid);
    int stat;
    while (1) {
      pid_t p = waitpid(pid, &stat, WUNTRACED);
      if (p < 0) break;

      // printf("stat = %x\n", stat);

      if (WIFSIGNALED(stat)) {
        printf("Signaled %s\n", signal_str(WTERMSIG(stat)));
        break;
      } else if (WIFEXITED(stat)) {
        printf("Exited %d\n", WEXITSTATUS(stat)); 
        break;
      } else if (WIFSTOPPED(stat)) {
        printf("Stopped %s\n", signal_str(WSTOPSIG(stat)));

        // Hopefully PTRACE_GETSIGINFO will give us arch info
        // Sadly it won't.
        // But it helps to tell "int 0x3" TRAP from ptrace TRAPs
        siginfo_t siginfo;
        memset(&siginfo, 0, sizeof(siginfo_t));
        ptrace(PTRACE_GETSIGINFO, pid, NULL, &siginfo);
        // print_siginfo(siginfo);
        if (siginfo.si_pid == 0) {
          printf("  NOT PTRACE TRAP! Maybe \"int 0x3\"\n");
        }


        struct user context;
        ptrace(PTRACE_GETREGS, pid, NULL, &context.regs);
        printf("  rax = %llu\n", context.regs.orig_rax);

        // Read instructions, check if it is "syscall"
        long inst;
        inst = ptrace(PTRACE_PEEKTEXT, pid, context.regs.rip - 2, NULL);
        // asm: "0f 05" is "syscall"
        if ((inst & 0xffff) != 0x050f) { // assuming little endian
          printf("  NOT syscall! Maybe \"int 0x80\" or \"sysenter\". Potential dangerous!\n");
          // should make it "Runtime Error"
        } else {
          printf("  Normal syscall\n");
        }

        ptrace(PTRACE_SYSCALL, pid, NULL, 0);
      } else {
        printf("Unexpected stat\n");
      }
    }
  } else {
    // child
    ptrace(PTRACE_TRACEME);
    execl("./hello", "hello", NULL);
    puts("Failed to exec");
  }
  
  return 0;
}
